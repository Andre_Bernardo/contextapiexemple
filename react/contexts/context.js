import React from 'react';

/**
 * Caso não defina um provider, este é o valor que o context vai conter
 */
const defaultContext = {
	text: 'None',
	changer: () => {
		console.log('Default Context');
	},
};

export const MyContext = React.createContext(defaultContext);
