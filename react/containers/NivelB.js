import React from 'react';
import { MyContext } from '../contexts/context';

class NivelB extends React.Component {
	constructor(props) {
		super(props);

		this.checkContext = this.checkContext.bind(this);
		this.clickHandler = this.clickHandler.bind(this);
	}

	checkContext() {
		console.log(this.context);
	}

	/**
	 *
	 * @param {Event} e
	 */
	clickHandler(e) {
		e.preventDefault();
		this.context.changer();
	}

	render() {
		this.checkContext();
		return (
			<React.Fragment>
				<div className='wrapper-nivelB'>
					<p>
						Olá pá > Sou do nivel B - O valor contido no Pai é:
						{this.context.text}
					</p>
					<button onClick={this.clickHandler}>Altera Principal</button>
				</div>
			</React.Fragment>
		);
	}
}

/**
 * Definir qual o context que eu quero que este componente aceda.
 * É possivel aceder a mais do que um, mas isso parece-me demasiado complicado
 */
NivelB.contextType = MyContext;

export { NivelB };
