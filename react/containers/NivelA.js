import React from 'react';
import { NivelB } from './NivelB';

class NivelA extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<React.Fragment>
				<div className='wrapper-nivelA'>
					<p>Olá pá > Sou do nivel A</p>
					<NivelB />
					<NivelB />
				</div>
			</React.Fragment>
		);
	}
}

export { NivelA };
