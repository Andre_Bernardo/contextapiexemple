import React from 'react';
import ReactDOM from 'react-dom';
import { NivelA } from './react/containers/NivelA';
import { MyContext } from './react/contexts/context';

class Principal extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			contextValue: 'Olá',
		};

		this.changeContextValue = this.changeContextValue.bind(this);
	}

	/**
	 * Função que altera o valor / do estado do componente Principal
	 */
	changeContextValue() {
		let value = this.state.contextValue;
		if (value === 'Olá') {
			value = 'Adeus';
		} else {
			value = 'Olá';
		}
		this.setState({ contextValue: value });
	}

	render() {
		const value = this.state.contextValue;

		/**
		 * Construção do objeto que será partilhado no context
		 */
		const contextContent = {
			text: value,
			changer: this.changeContextValue,
		};

		//Ao definir este Provider, estou a dar "overwrite sobre o valor por defeito"
		return (
			<MyContext.Provider value={contextContent}>
				<div className='wrapper-app'>
					<p> {value + ' mundo'} </p>
					<NivelA />
				</div>
			</MyContext.Provider>
		);
	}
}

const domContainer = document.querySelector('#app');
ReactDOM.render(React.createElement(Principal), domContainer);
